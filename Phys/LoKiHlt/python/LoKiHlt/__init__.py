###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# $Id: __init__.py,v 1.1.1.1 2008-09-21 14:41:20 ibelyaev Exp $
# =============================================================================
"""
Helper python module for LoKiHlt package
"""
# =============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@nikhef.nl"
__version__ = " CVS Tag $Name: not supported by cvs2svn $, version $Revision: 1.1.1.1 $  "
# =============================================================================
# The END
# =============================================================================
