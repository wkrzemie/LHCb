/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Log: not supported by cvs2svn $
// Revision 1.3  2007/02/28 18:31:19  marcocle
// Replaced "static const CLID&" with "static const CLID"
//
// Revision 1.2  2001/06/29 10:26:17  ibelyaev
// update to use new features of DetDesc v7 package
//
#ifndef CALO_CLID_DESUBSUBCALORIMETER_H
#define CALO_CLID_DESUBSUBCALORIMETER_H 1
#include "GaudiKernel/ClassID.h"

// External declarations
static const CLID CLID_DeSubSubCalorimeter = 8902;

#endif // CALO_CLID_DESUBSUBCALORIMETER_H
