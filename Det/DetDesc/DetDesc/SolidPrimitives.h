/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef __DETDESC_SOLID_SOLIDPRIMITIVES_H__
#define __DETDESC_SOLID_SOLIDPRIMITIVES_H__

#include "DetDesc/SolidBox.h"
#include "DetDesc/SolidCons.h"
#include "DetDesc/SolidPolycone.h"
#include "DetDesc/SolidSphere.h"
#include "DetDesc/SolidTrap.h"
#include "DetDesc/SolidTrd.h"
#include "DetDesc/SolidTubs.h"

#endif //  __DETDESC_SOLID_SOLIDPRIMITIVES_H__
