/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CLIDVeloAlignCond.h,v 1.1 2008-07-11 16:35:58 marcocle Exp $
#ifndef VELODET_CLIDVELOALIGNCOND_H
#define VELODET_CLIDVELOALIGNCOND_H 1
#include "GaudiKernel/ClassID.h"

/** Class Id for VeloAlignCond.
 *  @author Marco Clemencic
 *  @date   2008-06-23
 */
static const CLID CLID_VeloAlignCond = 1008106;

#endif // VELODET_CLIDVELOALIGNCOND_H
