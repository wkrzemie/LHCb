/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: lcgDict.h,v 1.1.1.1 2009-02-16 16:04:17 panmanj Exp $
#ifndef LUMIEVENT_LCGDICT_H
#define LUMIEVENT_LCGDICT_H 1

// Additional classes to be added to automatically generated lcgdict

// begin include files
#include "Event/HltLumiSummary.h"
#include "Event/LumiCounters.h"
#include "Event/LumiFSR.h"

// end include files

#endif // LUMIEVENT_LCGDICT_H
