/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: lcg_selection.h,v 1.4 2010-03-17 21:30:50 raaij Exp $
#ifndef FSREVENT_LCGDICT_H
#define FSREVENT_LCGDICT_H 1

// Additional classes to be added to automatically generated lcgdict

// begin include files
#include <map>
#include <string>
#include <utility>
// end include files

template std::map<std::string, unsigned long long>;

#endif // FSREVENT_LCGDICT_H
