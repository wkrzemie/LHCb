/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MuonDAQDict.h,v 1.1 2007-11-28 14:48:15 cattanem Exp $
#ifndef DICT_MUONDAQDICT_H
#define DICT_MUONDAQDICT_H 1

#include "MuonDAQ/IMuonRawBuffer.h"

#endif // DICT_MUONDAQDICT_H
